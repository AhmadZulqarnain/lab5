@extends('layouts.app')

@section('content')
<div>
<h1>Users List</h1>
<button>
<a href="user/create">Create</a>
</button>"
<table>
<tr>
<th>NAME</th><th>EMAIL</th><th>CREATED_AT</th><th>DEPARTMENT</th><th colspan="2">ACTION</th>
</tr>
@foreach($user as $usr)
<tr>
<td>{{$i++}}</td>
<td>{{$usr->name}}</td>
<td>{{$usr->email}}</td>
<td>{{$usr->created_at}}</td>
<td>{{$usr->department['name']}}</td>
<td>
<a href="{{route('user.show',$usr->id)}}">Details</a>
</td>
<td>

</td>
</tr>
</table>
</div>