@extends('layouts.app')

@section('content')
<button>
<a href="department/create">Create</a>
</button>
<table>
<tr>
<th>No.</th><th>Name</th><th>Action</th>
</tr>
@foreach($dep as $dept)
<tr>
<td>{{$i++}}</td>
<td>{{$dept->name}}</td>
<td><a href="{{route('department.edit',$dept->id)}}">Edit</a> &nbsp;
<a href="department/{{$dept->id}}">Delete</a>
</td>
</tr>
@endforeach
</table>
@endsection