@extends('layouts.app')

@section('content')
<form method="post" action="department">
<div class="container-fluid">
    <label for="name">Name</label>
    <input type="text" name="name" placeholder="Enter name">
  </div>
  <div class="container-fluid">
  <button type="submit" class="btn btn-primary">Save</button>
  </div>
</form>
@endsection