<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table)
		{
			$table->bigInteger('department_id')->unsigned()->index()->nullable();
			$table->foreign('department_id')->references()->on('departments');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table)
		{
			$table->dropForeign('department_id');
			$table->dropColumn('department_id');
		});
    }
}
